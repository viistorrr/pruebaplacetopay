@extends('layout')

@section('title', $title)

@section('content')

    <h1><b>Prueba Place To Pay</b></h1>

    <form action="{{ route('payment::start') }}" id="start" method="POST">
        {{ csrf_field() }}

        <div>
            <p for="account">Seleccionar el tipo de cuenta:</p>
        </div>
        <select name="accountCode">
            @foreach($accounts as $account)
                <option value="{{ $account['accountCode'] }}">{{ $account['accountType'] }}</option>
            @endforeach
        </select>
        <div>
            <p for="bank">Seleccionar la Entidad Bancaria:</p>
        </div>
        <div>
            <select name="bankCode">
                @foreach($ArrayOfBank as $bank)
                    <!--Banco Unión Colombiano para Pruebas-->
                    <option value="{{ $bank->bankCode }}">{{ $bank->bankName }}</option>
                @endforeach
            </select>
        </div>

        <div>
            <strong>Se asumirá el Pagador y Comprador son la misma persona:</strong>
        </div>

        <div class="row">
            <div class="column right">
                <span>Tipo de Documento:</span>
                <select name="documentType">
                    @foreach($documentsType as $documentType)
                        <option value="{{ $documentType['documentCode'] }}">{{ $documentType['documentType'] }}</option>
                    @endforeach
                </select>
            </div>
            <div class="column left">
                <span>Número de Documento:</span>
                <input type="number" name="document" min="0" value="">
            </div>
        </div>

        <div class="row">
            <div class="column right">
                <span>Nombre:</span>
                <input type="text" name="firstName" value="">
            </div>
            <div class="column left">
                <span>Apellido:</span>
                <input type="text" name="lastName" value="">
            </div>
        </div>

        <div class="row">
            <div class="column right">
                <span>Compañía:</span>
                <input type="text" name="company" value="">
            </div>
            <div class="column left">
                <span>Email:</span>
                <input type="email" name="emailAddress" value="">
            </div>
        </div>

        <div class="row">
            <div class="column right">
                <span>Dirección:</span>
                <input type="text" name="address" value="">
            </div>
            <div class="column left">
                <span>Ciudad:</span>
                <input type="text" name="city" value="">
            </div>
        </div>

        <div class="row">
            <div class="column right">
                <span>Departamento:</span>
                <input type="text" name="province" value="">
            </div>
            <div class="column left">
                <span>Pais:</span>
                <input type="text" name="country" value="CO" disabled>
            </div>
        </div>

        <div class="row">
            <div class="column right">
                <span>Teléfono Fijo:</span>
                <input type="text" name="phone" value="">
            </div>
            <div class="column left">
                <span>Celular:</span>
                <input type="text" name="mobile" value="">
            </div>
        </div>

        <div>
            <a href="{{ URL::previous() }}" class="button"><b>REGRESAR</b></a>
            <!--Send Transaction Form-->
            <a href="#" onclick="document.getElementById('start').submit()" class="button"><b>CONTINUAR</b></a>
        </div>
    </form>
@endsection

